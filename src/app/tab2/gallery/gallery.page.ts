import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.page.html',
  styleUrls: ['./gallery.page.scss'],
})
export class GalleryPage implements OnInit {
  imageResponse: any;
  exif:any
  public addEventPage: string = 'dayOne';

  constructor(private storage: Storage) { }

  ngOnInit() {
  }
  ionViewDidEnter() {
    this.storage.get('imageEXIF').then((exif) => {
      this.exif = exif || [];
    });

    this.storage.get('imageResponse').then((imageResponse) => {
      this.imageResponse = imageResponse || [];
    });
  }

}
