import { Component } from '@angular/core';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';

declare var EXIF: any;

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  imageResponse: any;
  options: any;



  constructor(private imagePicker: ImagePicker, private storage: Storage, private router: Router) {
  }

  ionViewDidEnter() {
    this.storage.get('imageResponse').then((imageResponse) => {
      this.imageResponse = imageResponse || [];
    });
  }

  ConvertDMSToDD(degrees, minutes, seconds, direction) {

    var dd = degrees + (minutes / 60) + (seconds / 3600);

    if (direction == "S" || direction == "W") {
      dd = dd * -1;
    }

    return dd;
  }

  getImages() {
    this.options = {
      // Android only. Max images to be selected, defaults to 15. If this is set to 1, upon
      // selection of a single image, the plugin will return it.
      //maximumImagesCount: 3,

      // max width and height to allow the images to be.  Will keep aspect
      // ratio no matter what.  So if both are 800, the returned image
      // will be at most 800 pixels wide and 800 pixels tall.  If the width is
      // 800 and height 0 the image will be 800 pixels wide if the source
      // is at least that wide.
      width: 200,
      //height: 200,

      // quality of resized image, defaults to 100
      quality: 25,

      // output type, defaults to FILE_URIs.
      // available options are 
      // window.imagePicker.OutputType.FILE_URI (0) or 
      // window.imagePicker.OutputType.BASE64_STRING (1)
      outputType: 1
    };
    // this.imageResponse = [];
    this.imagePicker.getPictures(this.options).then((results) => {
      for (var i = 0; i < results.length; i++) {
        this.imageResponse.push('data:image/jpeg;base64,' + results[i]);
        //this.test('data:image/jpeg;base64,' + results[i]);

        EXIF.getData(results[i], function () {
          var myData = this;
          console.log(myData.exifdata);
          var latDegree = myData.exifdata.GPSLatitude[0].numerator;
          var latMinute = myData.exifdata.GPSLatitude[1].numerator;
          var latSecond = myData.exifdata.GPSLatitude[2].numerator;
          var latDirection = myData.exifdata.GPSLatitudeRef;
          var latFinal = this.ConvertDMSToDD(latDegree, latMinute, latSecond, latDirection);
          console.log(latFinal);
          alert(latFinal);
          // Calculate longitude decimal
          var lonDegree = myData.exifdata.GPSLongitude[0].numerator;
          var lonMinute = myData.exifdata.GPSLongitude[1].numerator;
          var lonSecond = myData.exifdata.GPSLongitude[2].numerator;
          var lonDirection = myData.exifdata.GPSLongitudeRef;
          var lonFinal = this.ConvertDMSToDD(lonDegree, lonMinute, lonSecond, lonDirection);
          console.log(lonFinal);
          alert(lonFinal);

          this.storage.set('imageEXIF', myData);

        });

      }
      this.storage.set('imageResponse', this.imageResponse);


    }, (err) => {
      alert(err);
    }).then(() => {
      if (this.imageResponse.length > 2) {
        this.router.navigate(['/gallery']);
      }
    })
  }

  // loaded(e) {
  //   console.log('Image event-', e);
  //   console.log('Image-', e.target);

  // }

  // test(url) {
  //   exif.getData(url, function () {
  //     //alert(e.target);
  //     console.log('pic :', url);
  //     var allMetaData = exif.getAllTags(this);
  //     //alert(allMetaData);
  //     console.log(allMetaData);
  //   });
  // }

}
